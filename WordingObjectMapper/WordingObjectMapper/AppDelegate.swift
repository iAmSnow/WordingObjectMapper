//
//  AppDelegate.swift
//  WordingObjectMapper
//
//  Created by Apple on 19/12/2560 BE.
//  Copyright © 2560 ToOm Khunsri. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

